import dash
from dash import dcc, html
from dash.dependencies import Input, Output
import owncloud
import os

app = dash.Dash(__name__)


#Constants
public_link = 'https://owncloud.gwdg.de/index.php/s/csCAOgo5VqekcBr'
oc = owncloud.Client.from_public_link(public_link)




app.layout = html.Div([
    html.H1("Select a Manuscript"),
    dcc.Dropdown(
        id='manuscript-dropdown',
        options=[
            {'label': 'Manuscript 1', 'value': 'manuscript1'},
            {'label': 'Manuscript 2', 'value': 'manuscript2'}
        ]
    ),
    html.Div(id='image-display')
])

@app.callback(
    Output('image-display', 'children'),
    [Input('manuscript-dropdown', 'value')]
)
def display_image(selected_manuscript):
    if selected_manuscript == 'manuscript1':
        # Load images from owncloud
        # Maybe I can add a file deletion
        success = oc.get_file('/manuscript1.jpg', 'assets/current_img.jpg')
        filepath="current_img.jpg"
        print(success)
    elif selected_manuscript == 'manuscript2':
        success = oc.get_file('/manuscript2.jpg', 'assets/current_img2.jpg')
        filepath="current_img2.jpg"
        print(success)
    else:
        return "No manuscript selected"
    image = html.Img(src=f"assets/{filepath}")
    
    return image

if __name__ == '__main__':
    app.run_server(debug=True)